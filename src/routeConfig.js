import registrationLogTemplate from './templates/registrationLog.html!text';
import RegistratioinLogCtrl from './controllers/registrationLogCtrl';

import registrationInfoTemplate from './templates/registrationInfo.html!text';
import RegistrationInfoCtrl from './controllers/registrationInfoCtrl';

import submittedClaimsTemplate from './templates/submittedClaims.html!text';
import SubmittedClaimsCtrl from './controllers/submittedClaimsCtrl';

import submittedExtendedWarrantyTemplate from './templates/submittedExtendedWarranty.html!text';
import SubmittedExtendedWarrantyCtrl from './controllers/submittedExtendedWarrantyCtrl';


export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when('/',
                {
                    template:registrationLogTemplate,
                    controller: RegistratioinLogCtrl,
                    controllerAs:'ctrl'
                }
            ).when('/registrationInfo',
                {
                    template:registrationInfoTemplate,
                    controller: RegistrationInfoCtrl,
                    controllerAs:'ctrl'
                }
            ).when('/submittedClaims',
                {
                    template:submittedClaimsTemplate,
                    controller: SubmittedClaimsCtrl,
                    controllerAs:'ctrl'
                }
            ).when('/submittedExtendedWarranty',
                {
                    template:submittedExtendedWarrantyTemplate,
                    controller: SubmittedExtendedWarrantyCtrl,
                    controllerAs:'ctrl'
                }
            )
            .otherwise({
                redirectTo:"/"
            });

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];