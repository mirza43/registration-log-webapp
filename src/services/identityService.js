import SessionManager from 'session-manager';

/**
 * @class {IdentityService}
 */

export default class IdentityService {

    _$q;

    _sessionManager:SessionManager;

    constructor(
        $q,
        sessionManager : SessionManager
    ){        
        this.getAccessToken = function(){
            return $q( resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    ).catch(function(error){
                        console.log("error in IdentityService......", error);
                    })
            )
        }
    }
}

IdentityService.$inject = [
    '$q',
    'sessionManager'
];