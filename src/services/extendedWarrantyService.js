import ExtendedWarrantyServiceSdk from 'extended-warranty-service-sdk';

/**
 * @class {ExtendedWarrantyService}
 */

export default class ExtendedWarrantyService {
    _$q;

    _extendedWarrantyServiceSdk : ExtendedWarrantyServiceSdk;

    constructor(
        $q,

        extendedWarrantyServiceSdk : ExtendedWarrantyServiceSdk
    ){
        /**
         initialization
         */
        if(!$q){
            throw new TypeError('$q required');
        }
        this._$q = $q;
        
        if(!extendedWarrantyServiceSdk){
            throw new TypeError('extendedWarrantyServiceSdk required');
        }
        this._extendedWarrantyServiceSdk = extendedWarrantyServiceSdk;
    }
    /**
     methods
     */    
    getExtendedWarranty( partnerSaleRegistrationId , accessToken ){
        return this._$q(resolve =>
            this._extendedWarrantyServiceSdk
                .getExtendedWarrantyPurchase( partnerSaleRegistrationId , accessToken )
                .then(
                    response => resolve( response )
                ).catch(function(error){
                    console.log("error in ExtendedWarrantyService - getExtendedWarranty......", error);
                })
        )
    };
}

ExtendedWarrantyService.$inject = [
    '$q',
    'extendedWarrantyServiceSdk'
];