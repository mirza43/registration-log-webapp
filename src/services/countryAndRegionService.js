import Iso3166Sdk from 'iso-3166-sdk';

export default class countryAndRegionService {

    _$q;
    
    _iso3166Sdk : Iso3166Sdk;

    constructor(
        $q,
        iso3166Sdk : Iso3166Sdk
    ){        
        if(!$q){
            throw new TypeError("$q required");
        }
        this._$q = $q;
        
        if(!iso3166Sdk){
            throw new TypeError("iso3166Sdk required");
        }
        this._iso3166Sdk = iso3166Sdk;
    }
    /**
     methods
     */
    getCountryInfo( countryCode ){
        return this._$q(
            resolve =>
                this._iso3166Sdk
                    .listCountries()
                    .then( countries =>
                        {
                            countries.forEach(function( val , key ){
                                if(val.alpha2Code == countryCode){
                                    resolve( val.name )
                                }
                            })
                        }                                
                    )
        )
    };

    getRegionInfo( regionCode , countryCode ){
        return this._$q(resolve =>
            this._iso3166Sdk
                .listSubdivisionsWithAlpha2CountryCode( countryCode )
                .then(
                    regions => {
                        regions.forEach(function( val , key ){
                            if(val.code == regionCode){
                                resolve( val.name )
                            }
                        })
                    }
                )
        )
    };
}

countryAndRegionService.$inject = [
    '$q',
    'iso3166Sdk'
];