import ClaimSpiffServiceSdk from 'claim-spiff-service-sdk';

export default class ClaimSpiffService {

    _$q;
    _claimSpiffServiceSdk : ClaimSpiffServiceSdk;

    constructor(
        $q,
        claimSpiffServiceSdk : ClaimSpiffServiceSdk
    ){        
        this.getClaimSpiffInfo = function( registrationId, accessToken ){
            return $q( resolve =>
                claimSpiffServiceSdk
                .getEntitlementWithPartnerSaleRegistrationId( registrationId, accessToken )
                    .then(
                        claimSpiffs =>
                            resolve(claimSpiffs)
                    ).catch(function(error){
                        console.log("error in ClaimSpiffService......", error);
                    })
            )
        }
    }
}

ClaimSpiffService.$inject = [
    '$q',
    'claimSpiffServiceSdk'
];