/**
 * @class {PrepareClaimSpiffRequestFactory}
 */

export default class PrepareClaimSpiffRequestFactory {

    constructor(){
        
    }
    prepareClaimSpiffRequest( assetsList ){
        var request = {};
        
        request.simpleLineItems = [];
        
        request.compositeLineItems = [];
        
        if( assetsList.simpleLineItems.length ){
            assetsList.simpleLineItems.forEach(function( val , ind ) {
                request.simpleLineItems.push( {"serialNumber" : val.serialNumber.substr(0,4)} );
            });
        }
        
        if( assetsList.compositeLineItems.length ){
            assetsList.compositeLineItems.forEach(function( component , index ) {
                request.compositeLineItems.push( {"components" : []} );
                component.components.forEach(function( val , ind ) {
                    request.compositeLineItems[index].components.push( {"serialNumber" : val.serialNumber.substr(0,4)} );
                });
            });
        }
        
        return request;
    };
    
    static prepareClaimSpiffRequestFactory(){
        return new PrepareClaimSpiffRequestFactory();
    }
}

PrepareClaimSpiffRequestFactory.prepareClaimSpiffRequestFactory.$inject = [];