/**
 * @class {MergeSpiffDataWithAssetsFactory}
 */

export default class MergeSpiffDataWithAssetsFactory {    
    
    mergeSpiffDataWithAssets( assetsList , spiffData ){
        
        if( assetsList.simpleLineItems.length ){
            assetsList.simpleLineItems.forEach(( value , index ) => {
                if( spiffData.simpleSerialCode.length ){
                    spiffData.simpleSerialCode.forEach( ( val , ind ) => {
                        if( value.serialNumber.substr(0,4) == val.serialNumber ){
                            value.spiffAmount = val.amount;
                        }
                    });
                }
            });
        }
        if( assetsList.compositeLineItems.length ){
            assetsList.compositeLineItems.forEach(( component , index1 ) => {
                if( spiffData.compositeSerialCode.length ){
                    spiffData.compositeSerialCode.forEach(( value , index2 ) => {
                        var counter = 0;
                        component.components.forEach(( val1 , ind1 ) => {
                            value.components.forEach(( val2 , ind2 ) => {
                                if( val1.serialNumber.substr(0,4) == val2.serialNumber){
                                    counter++;
                                    if( counter == component.components.length ){
                                        counter = 0;
                                        component.spiffAmount = value.amount;
                                    }
                                }
                            });
                        });
                    });
                }
            });
        }
        
        return assetsList;
    };
    
    static mergeSpiffDataWithAssetsFactory(){
        return new MergeSpiffDataWithAssetsFactory();
    }
}

MergeSpiffDataWithAssetsFactory.mergeSpiffDataWithAssetsFactory.$inject = [];