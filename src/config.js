import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {RegistrationLogServiceSdkConfig} from 'registration-log-service-sdk';
import {PartnerSaleRegistrationDraftServiceSdkConfig} from 'partner-sale-registration-draft-service-sdk';
import {ConsumerSaleRegistrationServiceSdkConfig} from 'consumer-sale-registration-service-sdk';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import {AccountContactServiceSdkConfig} from 'account-contact-service-sdk';
import {BuyingGroupServiceSdkConfig} from 'buying-group-service-sdk';
import {ManagementCompanyServiceSdkConfig} from 'management-company-service-sdk';
import {CustomerSourceServiceSdkConfig} from 'customer-source-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {ExtendedWarrantyServiceSdkConfig} from 'extended-warranty-service-sdk';
import {SpiffMasterDataServiceSdkConfig} from 'master-data-service-sdk';

export default class Config {

    _identityServiceSdkConfig : IdentityServiceSdkConfig;

    _sessionManagerConfig : SessionManagerConfig;
    
    _registrationLogServiceSdkConfig : RegistrationLogServiceSdkConfig;
    
    _partnerSaleRegistrationDraftServiceSdkConfig : PartnerSaleRegistrationDraftServiceSdkConfig;
    
    _consumerSaleRegistrationServiceSdkConfig : ConsumerSaleRegistrationServiceSdkConfig;
    
    _accountServiceSdkConfig : AccountServiceSdkConfig;
    
    _accountContactServiceSdkConfig : AccountContactServiceSdkConfig;
    
    _buyingGroupServiceSdkConfig : BuyingGroupServiceSdkConfig;
    
    _managementCompanyServiceSdkConfig : ManagementCompanyServiceSdkConfig;
    
    _customerSourceServiceSdkConfig : CustomerSourceServiceSdkConfig;
    
    _partnerRepServiceSdkConfig : PartnerRepServiceSdkConfig;
    
    _claimSpiffServiceSdkConfig : ClaimSpiffServiceSdkConfig;
    
    _extendedWarrantyServiceSdkConfig : ExtendedWarrantyServiceSdkConfig;
    
    _spiffMasterDataServiceSdkConfig : SpiffMasterDataServiceSdkConfig;

    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                sessionManagerConfig : SessionManagerConfig,
                registrationLogServiceSdkConfig : RegistrationLogServiceSdkConfig,
                partnerSaleRegistrationDraftServiceSdkConfig : PartnerSaleRegistrationDraftServiceSdkConfig,
                consumerSaleRegistrationServiceSdkConfig : ConsumerSaleRegistrationServiceSdkConfig,
                accountServiceSdkConfig : AccountServiceSdkConfig,
                accountContactServiceSdkConfig : AccountContactServiceSdkConfig,
                buyingGroupServiceSdkConfig : BuyingGroupServiceSdkConfig,
                managementCompanyServiceSdkConfig : ManagementCompanyServiceSdkConfig,
                customerSourceServiceSdkConfig : CustomerSourceServiceSdkConfig,
                partnerRepServiceSdkConfig : PartnerRepServiceSdkConfig,
                claimSpiffServiceSdkConfig : ClaimSpiffServiceSdkConfig,
                extendedWarrantyServiceSdkConfig : ExtendedWarrantyServiceSdkConfig,
                spiffMasterDataServiceSdkConfig : SpiffMasterDataServiceSdkConfig
    ) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig required');
        }
        this._sessionManagerConfig = sessionManagerConfig;
        
        if (!registrationLogServiceSdkConfig) {
            throw new TypeError('registrationLogServiceSdkConfig required');
        }
        this._registrationLogServiceSdkConfig = registrationLogServiceSdkConfig;
        
        if (!partnerSaleRegistrationDraftServiceSdkConfig) {
            throw new TypeError('partnerSaleRegistrationDraftServiceSdkConfig required');
        }
        this._partnerSaleRegistrationDraftServiceSdkConfig = partnerSaleRegistrationDraftServiceSdkConfig;
        
        if (!consumerSaleRegistrationServiceSdkConfig) {
            throw new TypeError('consumerSaleRegistrationServiceSdkConfig required');
        }
        this._consumerSaleRegistrationServiceSdkConfig = consumerSaleRegistrationServiceSdkConfig;
        
        if (!accountServiceSdkConfig) {
            throw new TypeError('accountServiceSdkConfig required');
        }
        this._accountServiceSdkConfig = accountServiceSdkConfig;
        
        if (!accountContactServiceSdkConfig) {
            throw new TypeError('accountContactServiceSdkConfig required');
        }
        this._accountContactServiceSdkConfig = accountContactServiceSdkConfig;
        
        if (!buyingGroupServiceSdkConfig) {
            throw new TypeError('buyingGroupServiceSdkConfig required');
        }
        this._buyingGroupServiceSdkConfig = buyingGroupServiceSdkConfig;
        
        if (!managementCompanyServiceSdkConfig) {
            throw new TypeError('ManagementCompanyServiceSdkConfig required');
        }
        this._managementCompanyServiceSdkConfig = managementCompanyServiceSdkConfig;
        
        if (!customerSourceServiceSdkConfig) {
            throw new TypeError('customerSourceServiceSdkConfig required');
        }
        this._customerSourceServiceSdkConfig = customerSourceServiceSdkConfig;
        
        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }
        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;
        
        if (!claimSpiffServiceSdkConfig) {
            throw new TypeError('claimSpiffServiceSdkConfig required');
        }
        this._claimSpiffServiceSdkConfig = claimSpiffServiceSdkConfig;
        
        if (!extendedWarrantyServiceSdkConfig) {
            throw new TypeError('extendedWarrantyServiceSdkConfig required');
        }
        this._extendedWarrantyServiceSdkConfig = extendedWarrantyServiceSdkConfig;
        
        if (!spiffMasterDataServiceSdkConfig) {
            throw new TypeError('spiffMasterDataServiceSdkConfig required');
        }
        this._spiffMasterDataServiceSdkConfig = spiffMasterDataServiceSdkConfig;
    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }

    get registrationLogServiceSdkConfig(){
        return this._registrationLogServiceSdkConfig;
    }
    
    get partnerSaleRegistrationDraftServiceSdkConfig(){
        return this._partnerSaleRegistrationDraftServiceSdkConfig;
    }
    
    get consumerSaleRegistrationServiceSdkConfig(){
        return this._consumerSaleRegistrationServiceSdkConfig;
    }
    
    get accountServiceSdkConfig(){
        return this._accountServiceSdkConfig;
    }
    
    get accountContactServiceSdkConfig(){
        return this._accountContactServiceSdkConfig;
    }
    
    get buyingGroupServiceSdkConfig(){
        return this._buyingGroupServiceSdkConfig;
    }
    
    get managementCompanyServiceSdkConfig(){
        return this._managementCompanyServiceSdkConfig;
    }
    
    get customerSourceServiceSdkConfig(){
        return this._customerSourceServiceSdkConfig;
    }
    
    get partnerRepServiceSdkConfig(){
        return this._partnerRepServiceSdkConfig;
    }
    
    get claimSpiffServiceSdkConfig(){
        return this._claimSpiffServiceSdkConfig;
    }
    
    get extendedWarrantyServiceSdkConfig(){
        return this._extendedWarrantyServiceSdkConfig;
    }
    
    get spiffMasterDataServiceSdkConfig(){
        return this._spiffMasterDataServiceSdkConfig;
    }
}